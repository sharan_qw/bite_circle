<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function dish()
    {	

    	return $this->hasMany('App\Dish');
    }

    protected $fillable = [
        'id', 'name',
    ];
    Protected $table = 'categories';
}
