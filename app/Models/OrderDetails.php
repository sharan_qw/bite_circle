<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetails extends Model
{
    
    protected $fillable = ['order_id', 'dish_id', 'quantity', 'price'];
}
