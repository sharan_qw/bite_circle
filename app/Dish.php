<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dish extends Model
{
    public function category()
    {

    	return $this->belong_to('App\Category');
  //   	$category = Category::where('name', $name)->first();
		// $dishes_collection = $category->dishes()->get();

    }

    protected $fillable = [
        'id', 'category_id', 'name','description','price','status'
    ];

protected $table = 'dishes';
}
