<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        return view('dashboard');

    }

   /* public function zip(Request $request)
    {
        return view('profile');

    }*/

    public function profile(Request $request)
    {
        return view('profile');

    }



    public function contactus(Request $request)
    {
        return view('contactus');

    }
    public function about(Request $request)
    {
        return view('about');

    }
}
