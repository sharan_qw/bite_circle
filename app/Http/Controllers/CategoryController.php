<?php

namespace App\Http\Controllers;

use App\Dish;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

//use DB;
//use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{
    public function dishes()
    {
        $categories = DB::table('categories')->get();
        return view('dishes', ['categories' => $categories]);
    }

    public function create(Request $request)
    {
        $file = $request ->file("image");
        if($request->hasFile("image")){
            $file->move("upload/",$file->getClientOriginalName());
//            $file->storeAs("public/",$file->getClientOriginalName());
//            Storage::putFile("public",$request ->file("image"));
        }

        $obj = new Dish;
        $obj->category_id = $request->category_id;
        $obj->name = $request->name;
        $obj->description = $request->description;
        $obj->price = $request->price;
        $obj->status = $request->status;
        $obj->image = $file->getClientOriginalName();
        $obj->save();
        return redirect('showlist');
    }

    public function showlist()
    {
        $dishes = DB::table('dishes')
            ->join('categories', 'dishes.category_id', '=', 'categories.id')
            ->select(['dishes.*', 'categories.name as category_name'])
            ->get();
//        dd($dishes);
        return view('showlist', ['dishes' => $dishes]);
    }

    public function edit($id)
    {
        $dish = Dish::find($id);
        $categories = DB::table('categories')->get();
        return view('edit', compact("dish"), ['categories' => $categories]);

    }

    public function update(Request $request,$id)
    {
        $dish = Dish::find($id);
        $dish->category_id = $request->category_id;
        $dish->name = $request->name;
        $dish->description = $request->description;
        $dish->price = $request->price;
        $dish->status = $request->status;
        if($request->hasFile("image")){
            $file = $request ->file("image");
            $file->move("upload/",$file->getClientOriginalName());
            $dish->image = $file->getClientOriginalName();
        }

        $dish->save();

        return redirect("showlist");
    }



}
