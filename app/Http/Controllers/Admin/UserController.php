<?php

namespace App\Http\Controllers\Admin;
use App\Models\PlacedOrders;
use App\Models\OrderDetails;
use App\Dish;
use App\Order_detail;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Image;

class UserController extends Controller
{
    public  function _construct(){
        $this->middleware("admin.auth");
    }
    public function last_order()
    {
        $placed_orders = DB::table('placed_orders')
            ->join('users','placed_orders.user_id','=','users.id')
            ->select(['placed_orders.*' , 'users.name as user_name' ])
        ->paginate(6);
        return view('admin/last_order', ['placed_orders' => $placed_orders]);
    }

    public function order_details($id)
    {
        $order_details = OrderDetails::where('order_id','=', $id)
            ->join('dishes', 'order_details.dish_id', '=', 'dishes.id')
            ->join('placed_orders as po', 'order_details.order_id', '=', 'po.id')
            ->join('users as u', 'po.user_id', '=', 'u.id')
            ->select(['order_details.*', 'dishes.name as dish_name' , 'po.user_id as user' , 'u.name as username' ])
            ->get();

        $user = $order_details->first->username;
        $total = 0;
        foreach ($order_details as $order) {
            $subtotal = $order['price'] * $order['quantity'];
            $total += $subtotal;
        }
         return view('admin/order_details', ['order_details' => $order_details,'total' => $total , 'user' => $user ]);
    }

    public function order_details_new($id)
    {

        $order_details = OrderDetails::where('order_id', '=', $id)
            ->join('dishes', 'order_details.dish_id', '=', 'dishes.id')
            ->join('placed_orders as po', 'order_details.order_id', '=', 'po.id')
            ->join('users as u', 'po.user_id', '=', 'u.id')
            ->select(['order_details.*', 'dishes.name as dish_name', 'po.user_id as user', 'u.name as username','po.id as pid'])
            ->get();

        $user = $order_details->first->username;
        $total = 0;
        foreach ($order_details as $order) {
            $subtotal = $order['price'] * $order['quantity'];
            $total += $subtotal;
        }
        return view('admin/registered/order_details_new', ['order_details' => $order_details, 'total' => $total, 'user' => $user]);

    }
    public function order_details_old($id)
    {
        $placed_orders = PlacedOrders::where('user_id', '=', $id)
            ->join('order_details', 'order_details.order_id', '=' , 'placed_orders.id')
            ->select('placed_orders.*')
            ->distinct()
            ->get();
        return view('admin/registered/order-details-old', ['placed_orders' => $placed_orders]);

    }

    public function place_order(Request $request)
    {
        $placedOrder = new PlacedOrders();
        $placedOrder->user_id = Auth::user()->id;

        $placedOrder->save();
        $dish = $request->input('dish');

        foreach ($dish as $value) {
            $singleDish = new OrderDetails();
            $singleDish->order_id = $placedOrder->id;
            $singleDish->dish_id = $value["id"];
            $singleDish->quantity = $value["quantity"];
            $singleDish->price = $value["price"];
            $singleDish->total = $value["total"];
            $singleDish->save();
        }

        return response()->json(['status' => 'success', 'message' => 'Placed Successfully']);
    }

    public function get_order()
    {
        return PlacedOrders::with('details')
//               ->where(user_id,'=', Auth::user()->id)
            ->get();
    }
    public function Registered()
    {
        $users = DB::table('users')
            ->paginate(6);
//  dd($users);

        return view('admin.registered.index', ['users' => $users]);
    }
}
