<?php

namespace App\Http\Controllers\Admin;

use App\Dish;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Symfony\Contracts\Cache\CacheTrait;

//use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{
    public function create()
    {
        $categories = DB::table('categories')->get();
        return view('admin.dishes.create', ['categories' => $categories]);
    }

    public function store(Request $request)
    {
        $file = $request ->file("image");
        if($request->hasFile("image")){
            $file->move("upload/",$file->getClientOriginalName());
//            $file->storeAs("public/",$file->getClientOriginalName());
//            Storage::putFile("public",$request ->file("image"));
        }

        $obj = new Dish;
        $obj->category_id = $request->category_id;
        $obj->name = $request->name;
        $obj->description = $request->description;
        $obj->price = $request->price;
        $obj->status = $request->status;
        $obj->image = $file->getClientOriginalName();
        $obj->save();
        return redirect('admin/dishes');
    }

    public function show(Dish $dish)
    {
        //
    }


    public function index()
    {
        $dishes = DB::table('dishes')
            ->join('categories', 'dishes.category_id', '=', 'categories.id')
            ->select(['dishes.*', 'categories.name as category_name'])
            ->paginate(3);

//        dd($dishes);

        return view('admin.dishes.index', ['dishes' => $dishes]);
    }

    public function edit($id)
    {
        $dish = Dish::find($id);
        $categories = DB::table('categories')->get();
        return view('admin.dishes.edit', compact("dish"), ['categories' => $categories]);

    }

    public function update(Request $request,$id)
    {
        $dish = Dish::find($id);
        $dish->category_id = $request->category_id;
        $dish->name = $request->name;
        $dish->description = $request->description;
        $dish->price = $request->price;
        $dish->status = $request->status;
        if($request->hasFile("image")){
            $file = $request ->file("image");
            $file->move("upload/",$file->getClientOriginalName());
            $dish->image = $file->getClientOriginalName();
        }

        $dish->save();

        return redirect("admin/dishes");
    }

    public function cat_update(Request $request,$id){
        $category = Category::find($id);
        $category->name = $request->name;
        $category->save();
        return redirect('admin/category')->with('success','Category Succesfully Edited');
    }
    public function cat_edit(Request $request,$id){
        $categories = Category::find($id);
        return view('admin.category.edit',  ['categories' => $categories]);
    }
    public function destroy($id) {
        $dish = Dish::find($id);
        $dish->delete();

        return redirect('admin/dishes')->with('success', 'item deleted!');
    }
    public function cat_destroy($id) {
        $category = Category::find($id);
        $category->delete();

        return redirect('admin/category')->with('success', 'Category deleted!');
    }
    public function make(Request $request)
    {
        $category = new Category();
        $category->name = $request->name;
        $category->save();
        return redirect('admin/category');
    }
    public function main()
    {
        $category = DB::table('categories')->paginate(4);
            //->get();
//        dd($category);

        return view('admin.category.index', ['category' => $category]);
    }
    public function create_cat()
    {
        return view('admin.category.create');
    }


}


