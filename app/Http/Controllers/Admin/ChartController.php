<?php
namespace App\Http\Controllers\Admin;
use App\Models\OrderDetails;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Charts;
use App\User;
use DB;


class ChartController extends Controller
{
    public function index()
    {
        return view('admin.dashboard.index');

    }
    public function order_chart()
    {
        $order_details = OrderDetails::where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),date('Y'))
            ->get();
        $chart = Charts::database($order_details, 'bar', 'highcharts')
            ->title("Total Monthly orders")
            ->elementLabel("Total orders")
            ->dimensions(1000, 500)
            ->responsive(false)
            ->groupBymonth(date('Y'), true);
        return view('admin.dashboard.order_chart',compact('chart'));

    }
    public function users_chart()
    {
        $users = User::where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),date('Y'))
            ->get();
        $chart = Charts::database($users, 'bar', 'highcharts')
            ->title("Monthly new Register Users")
            ->elementLabel("Total Users")
            ->dimensions(1000, 500)
            ->responsive(false)
            ->groupByMonth(date('Y'), true);
        return view('admin.dashboard.user_chart',compact('chart'));

    }
}
