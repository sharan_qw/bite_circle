<?php

namespace App\Http\Controllers\API;

use App\order_detail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\order;
use App\favourite;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public $successStatus = 200;

    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {


        $user = User::where('email', '=', request('email'))->get();
        if (count($user) > 0) {
            if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
                $user = Auth::user();

                $success['token'] = $user->createToken('MyApp')->accessToken;
                return response()->json(['status' => '200', 'response' => $success, 'error' => ''], $this->successStatus);
            } else {
                return response()->json(['error' => 'Invalid Password', 'status' => '401'], 401);
            }
        } else {
            return response()->json(['error' => 'Invalid Email', 'response' => '', 'status' => '401'], 400);
        }

    }

    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] = $user->createToken('MyApp')->accessToken;
        $success['name'] = $user->name;
        return response()->json(['success' => $success], $this->successStatus);
    }


    /**
     * details api
     *
     * @return \Illuminate\Http\Response
     */
    public function details()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this->successStatus);
    }

    public function payment(Request $request)
    {
        //  dd($request->all());
//        $pay = new order;
//        $pay->user_id = Auth::user()->id;
//
//        $pay-> status= request('status');
//        $pay->save();
//        return response()->json(['success' => $pay]);
//

        $validationArray = array();
        $validationArray['status'] = 'required';
        $validator = Validator::make($request->all(), $validationArray);
        if ($validator->fails()) {
            $response['message'] = "errors";
            $response['errors'] = $validator->errors()->toArray();
            $response['errors_key'] = array_keys($validator->errors()->toArray());
            return response()->json($response, 422);
        } else {
            $pay = new order;
            $pay->user_id = Auth::user()->id;
            $pay->status = request('status');
            $pay->save();

            if (!empty($pay)) {
                $response['message'] = "Data Insert";
                $response['order'] = $pay;
                return response()->json($response, 200);
            } else {
                $response['message'] = "OOPS! Something went wrong";
                return response()->json($response, 400);
            }
        }

    }

    public function favourite(Request $request)
    {

        $validationArray = array();
        $validationArray['title'] = 'required';
        $validationArray['rating'] = 'required';
        $validationArray['price'] = 'required';
        $validationArray['preparation_time'] = 'required';
        $validationArray['icon'] = 'required';
        $validationArray['photo'] = 'required';

        $validator = Validator::make($request->all(), $validationArray);
        if ($validator->fails()) {
            $response['message'] = "errors";
            $response['errors'] = $validator->errors()->toArray();
            $response['errors_key'] = array_keys($validator->errors()->toArray());
            return response()->json($response, 422);
        } else {
            $fav = new favourite();
            $fav->title = request('title');
            $fav->rating = request('rating');
            $fav->price = request('price');
            $fav->preparation_time = request('preparation_time');
            $fav->icon = request('icon');
            if ($request->file('photo')) {
                $file = $request->file('photo');
                $filename = time() . '.' . $file->getClientOriginalExtension();
                $file->move('storage/image', $filename);
                $pathimg = 'storage/image/' . $filename;
            }
            if (!empty($pathimg)) {
                $fav->photo = $pathimg;
            }

            $fav->save();

            if (!empty($fav)) {
                $response['message'] = "Data Inserted";
                $response['favourite'] = $fav;
                $response['status'] = "sucessfully";
                return response()->json($response, 200);
            } else {
                $response['message'] = "OOPS! Something went wrong";
                return response()->json($response, 400);
            }
        }
    }

    public function FavoriteGET(Request $request)
    {
        $fav = favourite::all();
        if (!empty($fav)) {
            $response['message'] = "Data Inserted";
            $response['response'] = $fav;
            $response['status'] = "sucessfully";
            return response()->json($response, 200);
        } else {
            $response['message'] = "OOPS! Something went wrong";
            return response()->json($response, 400);
        }

    }

    public function update(Request $request)
    {
        $pay = DB::table('order_detail')
            ->where('user_id', 1)
            ->update(['status' => 'success']);


        //  $pay-> status= request('status');
        //  $pay->save();
        return response()->json(['success' => $pay]);
    }


}
