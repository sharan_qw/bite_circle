<?php

namespace App\Http\Controllers;

use App\Addtocart;
use App\Category;
use App\ContactUs;
use App\Models\OrderDetails;
use App\Models\PlacedOrders;
use App\order_detail;
use Illuminate\Http\Request;
use App\Dish;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
// use App\Http\Controllers\Post;
use Image;
use Illuminate\Contracts\Session\Session;
use App\User;
use Validator;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Redirect;
use Stripe\Error\Card;
use Cartalyst\Stripe\Stripe;

class UserController extends Controller
{
    public  function _construct(){
        $this->middleware('auth');
    }




    public function addcart(Request $request)
    {

        $add = new Addtocart();
        $name = $request->input('name');
        $description = $request->input('description');
        $quantity = $request->input('quantity');
        $price = $request->input('price');

        $add->save();
        return view('home');
    }
    public function index(Request $request)
    {

        $dish = new dish();

        $name = $request->input('name');
        $description = $request->input('description');
        $quantity = $request->input('quantity');
        $price = $request->input('price');
        $staus = $request->input('status');
        $image = $request->input('image');
        $dish->save();
        return $dish;
    }


    public function get_new(Request $request)
    {
//        if ($request->input('name')) {
//            $dish = Dish::where('name', 'like', '%' . Input::get('name') . '%')
//                -> where ("dishes.status", 1)
//                /*                ->where('categories', 'like', '%' . Input::get('categories') . '%')*/
//                ->get();


        if ($request->input('id')) {



            $dish = DB::table('dishes')
                ->leftJoin('categories', 'dishes.category_id', '=', 'categories.id')
                ->select(['dishes.*', 'categories.name as categories_name', 'categories.id as categories_id'])
                ->where("dishes.category_id", '=', $request->input('id'))
                ->get();


            return response()->json($dish);
        }
    }

    public function record(Request $request)
    {
        if ($request->input('name')) {
            $dish = Dish::where('name', 'like', '%' . Input::get('name') . '%')
                -> where ("dishes.status", 1)
/*                ->where('categories', 'like', '%' . Input::get('categories') . '%')*/
                ->get();

        } else {
            $dish = DB::table('dishes')
            ->leftJoin('categories', 'dishes.category_id', '=', 'categories.id')
                ->select(['dishes.*', 'categories.name as categories_name'])
                ->where("dishes.status", 1)
                ->get();
        }
        return response()->json($dish);
    }



    public function dishesshow($id)
    {
        $categories= DB::table('categories')->get();
        $dishes = DB::table('dishes')
            ->where('dishes.category_id','=',$id)
            ->get();

        return view('cat_dish_cat',['categories'=>$categories],['dishes'=>$dishes]);
    }


    public function getshow()
    {

 $categories = DB::table('categories')->get();

            return view('cat_dish_index',['categories' =>$categories]);
    }

//    public function get_new(Request $request,$id)
//    {
//        if ($request->input('name')) {
//            $dish = Dish::where('name', 'like', '%' . Input::get('name') . '%')
//                -> where ("dishes.status", 1)
//                /*                ->where('categories', 'like', '%' . Input::get('categories') . '%')*/
//                ->get();
//
//        } else {
//            $dish = DB::table('dishes')
//                ->leftJoin('categories', 'dishes.category_id', '=', 'categories.id')
//                ->select(['dishes.*', 'categories.name as categories_name'])
//                ->where("dishes.status", 1)
//                ->get();
//        }
//        return response()->json($dish);
////        if ($request->input('name')) {
////            $dish = Dish::where('name', 'like', '%' . Input::get('name') . '%')
////                -> where ("dishes.status", 1)
////                /*                ->where('categories', 'like', '%' . Input::get('categories') . '%')*/
////                ->get();
////
////        } else {
////            $dish = DB::table('dishes')
////                ->leftJoin('categories', 'dishes.category_id', '=', 'categories.id')
////                ->select(['dishes.*', 'categories.name as categories_name'])
////                ->where('dishes.status' ,1)/*("dishes.category_id",'=', $id)*/
////                ->get();
////        }
////        return response()->json($dish);
//    }


    public function record_cat(Request $request)
    {
            $cat = DB::table('categories')
                ->select('categories.name as categories_name')
                ->get();

        return response()->json($cat);
    }

    public function profile()
    {
        return view('profile', array('user' => Auth::user()));

    }

    public function profile_image(Request $request)
    {
        return view('/contactus');
    }

    public function place_order(Request $request)
    {



        $placedOrder = new PlacedOrders();
        $placedOrder->user_id = Auth::user()->id;
        $placedOrder->save();

        DB::table('users')->where('id', Auth::user()->id)->increment('no_of_orders',1);

        $dish = $request->input('dish');

        foreach ($dish as $value) {
            $singleDish = new OrderDetails();
            $singleDish->order_id = $placedOrder->id;

            $singleDish->dish_id = $value["id"];
            $singleDish->quantity = $value["quantity"];
            $singleDish->price = $value["price"];
            $singleDish->save();
        }


        return response()->json(['status' => 'success', 'message' => 'Placed Successfully']);


    }

    public function get_order(Request $request)
    {
        return PlacedOrders::with('details')
            ->where('user_id', '=', Auth::user()->id)
            ->get();
    }

    public function order_history()
    {
        return view('orderhistory', array('user' => Auth::user()));
    }

    public function order_lastorder($id)
    {
        $order_details = OrderDetails::where('order_id', '=', $id)
            ->leftJoin('dishes', 'order_details.dish_id', '=', 'dishes.id')
            ->select(['order_details.*', 'dishes.name as dish_name'])
            ->get();
        $order_details->user_id = Auth::user()->id;
        return response()->json($order_details);
    }


    public function contact(Request $request)
    {
        {
            $contact = new ContactUs();

            $contact->name = $request->name;
            $contact->email = $request->email;
            $contact->message = $request->message;

            $contact->save();
            return $contact;
//            return redirect('/contactus');
        }


    }
    
    public function payment()
    {
        return view('payment', array('user' => Auth::user()));
    }


    public function payWithStripe()
    {
        return view('paywithstripe');
    }
    public function postPaymentWithStripe(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'card_no' => 'required',
            'ccExpiryMonth' => 'required',
            'ccExpiryYear' => 'required',
            'cvvNumber' => 'required',
            //'amount' => 'required',
        ]);
        $input = $request->all();
        if ($validator->passes()) {
            $input = array_except($input,array('_token'));
            $stripe = Stripe::make('sk_test_Qt5AH6ZDynvs56k0NtUnyhwg');
            try {
                $token = $stripe->tokens()->create([
                    'card' => [
                        'number' => $request->get('card_no'),
                        'exp_month' => $request->get('ccExpiryMonth'),
                        'exp_year' => $request->get('ccExpiryYear'),
                        'cvc' => $request->get('cvvNumber'),
                    ],
                ]);
//                 $token = $stripe->tokens()->create([
//                 'card' => [
//                 'number' => '4242424242424242',
//                 'exp_month' => 10,
//                 'cvc' => 314,
//                 'exp_year' => 2020,
//                 ],
//                 ]);
                if (!isset($token['id'])) {
                    return redirect()->route('addmoney.paywithstripe');
                }
                $charge = $stripe->charges()->create([
                    'card' => $token['id'],
                    'currency' => 'USD',
                    'amount' => 10.49,
                    'description' => 'Add in wallet',
                ]);

                if($charge['status'] == 'succeeded') {
                    /**
                     * Write Here Your Database insert logic.
                     */
                    echo "<pre>";
                    print_r($charge);exit();
                    return redirect()->route('addmoney.paywithstripe');
                } else {
                    \Session::put('error','Money not add in wallet!!');
                    return redirect()->route('addmoney.paywithstripe');
                }
            } catch (Exception $e) {
                \Session::put('error',$e->getMessage());
                return redirect()->route('addmoney.paywithstripe');
            } catch(\Cartalyst\Stripe\Exception\CardErrorException $e) {
                \Session::put('error',$e->getMessage());
                return redirect()->route('addmoney.paywithstripe');
            } catch(\Cartalyst\Stripe\Exception\MissingParameterException $e) {
                \Session::put('error',$e->getMessage());
                return redirect()->route('addmoney.paywithstripe');
            }
        }
    }


    public function pagi_next()
    {

        $dish = Dish::paginate(3);
        $response = [
            'pagination' => [
                'total' => $dish->total(),
                'per_page' => $dish->perPage(),
                'current_page' => $dish->currentPage(),
                'last_page' =>$dish->lastPage(),
                'from' => $dish->firstItem(),
                'to' => $dish->lastItem()
            ],
            'data' => $dish
        ];
        return response()->json($response);
    }

    public function pay()
    {
        return view('pay');

    }

}
