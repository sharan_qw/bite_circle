<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;

class AdminAuth
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Closure $next
     * @param string $guard
     * @return string
     */
    public function handle($request,Closure $next,$guard = 'admin')
    {
        if(!Auth::guard($guard)->check())
        {
            return redirect('admin');
        }
        return $next($request);
    }
}
