<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('details', 'API\UserController@details');
    Route::post('payment', 'API\UserController@payment');
    Route::post('favourite', 'API\UserController@favourite');

    Route::get('favourite-get','API\UserController@FavoriteGET');
    Route::post('update', 'API\UserController@update');

});


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
