<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();



//payment files
// route for to show payment form using get method
Route::get('pay/{id?}', 'RazorpayController@pay')->name('pay');

// route for make payment request using post method
Route::post('dopayment', 'RazorpayController@dopayment')->name('dopayment');
//chart route
Route::get('admin/dashboard-new', 'Admin\ChartController@index');
Route::get('admin/order_chart', 'Admin\ChartController@order_chart');
Route::get('admin/users_chart', 'Admin\ChartController@users_chart');






/*
Route::get('/chat', 'ChatsController@index');
Route::get('messages', 'ChatsController@fetchMessages');
Route::post('messages', 'ChatsController@sendMessage');*/



Route::get('/home', 'HomeController@index')->name('home');
Route::get('/record', 'UserController@record');
Route::post('/order', 'UserController@store');
Route::get('/profile', 'UserController@profile');
Route::get('/profile_image','UserController@profile_image');
Route::get('/data', 'UserController@data');
Route::get('/favourite', 'UserController@favourite');
Route::post('/image/store', 'ImageController@store');
//Route::get('/messages', 'UserController@fetch');


/*Route::get('/messages', 'MessageController@fetch');
Route::post('/messages', 'MessageController@sentMessage');*/

Route::get('/about', 'HomeController@about');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/profile', 'HomeController@profile');
Route::get('/contactus', 'HomeController@contactus');
Route::post('/contact', 'UserController@contact');

Route::get('/dish', 'UserController@index');
Route::post('/place-order', 'UserController@place_order');
Route::get('/get-order', 'UserController@get_order');
Route::post('/get-dish', 'UserController@record');
Route::get('/get-dishes', 'UserController@getshow');
Route::post('/get-dish_new', 'UserController@get_new');

Route::get('/dishes_show/{id}','UserController@dishesshow');

Route::post('/get-cat', 'UserController@record_cat');
Route::get('/order-details', 'UserController@get_order');
Route::get('/order-history/{id}', 'UserController@order_history');
Route::get('/get-lastorder/{id}', 'UserController@order_lastorder');


Route::post('addtocart','UserController@addcart');
//Route::get('/payment', 'UserController@payment');
//Route::get('/pay', 'UserController@pay');


/*Route::post('/search-filter', 'UserController@filter');*/
/*Route::get('/search-filter', 'UserController@filter1');*/
Route::get('addmoney/stripe', array('as' => 'addmoney.paywithstripe','uses' => 'MoneySetupController@payWithStripe'));
Route::post('addmoney/stripe', array('as' => 'addmoney.stripe','uses' => 'MoneySetupController@postPaymentWithStripe'));



Route::get('/pagi', 'UserController@pagi_next');




Route::get('admin/dashboard', 'AdminController@index')->name('admin.dashboard');;
Route::get('admin', 'Admin\LoginController@showLoginForm')->name('admin.login');
Route::post('admin', 'Admin\LoginController@login');
Route::post('admin-password/email', 'Admin\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
Route::get('admin-password/reset', 'Admin\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
Route::post('admin-password/reset', 'Admin\ResetPasswordController@reset');
Route::get('admin-password/reset/{token}', 'Admin\ResetPasswordController@showResetForm')->name('admin.password.reset');


Route::group(array('prefix' => 'admin','middleware'=>'admin.auth'), function () {
    Route::resource('cities', 'Admin\CityController');
    Route::get('dishes', 'Admin\CategoryController@index');
    Route::get('dishes/create', 'Admin\CategoryController@create');
    Route::post('dishes', 'Admin\CategoryController@store');
    Route::get('/dishes/{id}/edit','Admin\CategoryController@edit');
    Route::put('/dishes/update/{id}','Admin\CategoryController@update');
    Route::get('/last-order', 'Admin\UserController@last_order');
    Route::delete('/dishes/destroy/{id}','Admin\CategoryController@destroy');
    Route::get('/get-order', 'Admin\UserController@get_order');
    Route::get('/order-details/{id}', 'Admin\UserController@order_details');
    Route::get('/order_details_old/{id}', 'Admin\UserController@order_details_old');
    Route::get('/order_details_old/order_details_new/{id}', 'Admin\UserController@order_details_new');
    Route::post('category/make','Admin\CategoryController@make');
    Route::get('category','Admin\CategoryController@main');
    Route::get('category/create_cat','Admin\CategoryController@create_cat');
    Route::get('/category/{id}/edit','Admin\CategoryController@cat_edit');
    Route::put('/category/update/{id}','Admin\CategoryController@cat_update');
    Route::delete('/category/destroy/{id}','Admin\CategoryController@cat_destroy');
    Route::get('registered','Admin\UserController@Registered');

});


//Route::get('dishes','CategoryController@dishes');
//Route::post('dishes','CategoryController@create');
//Route::get('showlist','CategoryController@showlist');
//Route::get('/dishes/{id}/edit','CategoryController@edit');
//Route::put('/dishes/update/{id}','CategoryController@update');


