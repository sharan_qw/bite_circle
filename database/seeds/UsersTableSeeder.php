<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'laravel2',
            'email' => 'laravel2@laravel.com',
            'password' => Hash::make('laravel'),
        ]);
    }
}
