/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import Vue from 'vue'
/*
import  axios from "axios"
*/
import VueSwal from 'vue-swal'
import VueSweetalert2 from 'vue-sweetalert2'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
/*import '@fortawesome/fontawesome-free'
import 'font-awesome/scss/font-awesome.scss'*/

require('./bootstrap');
Vue.use(BootstrapVue);
Vue.use(VueSwal);
Vue.use(VueSweetalert2);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('profile-component', require('./components/ProfileComponent.vue').default);
Vue.component('contactus-component', require('./components/ContactusComponent.vue').default);
Vue.component('orderhistory-component', require('./components/OrderHistoryComponent.vue').default);
Vue.component('payment-component', require('./components/PaymentComponent.vue').default);
Vue.component('about-componen', require('./components/AboutComponent.vue').default);

Vue.component('pagination', require('laravel-vue-pagination'));
/*
Vue.component('chat-messages', require('./components/ChatMessages.vue').default);
*/
/*
Vue.component('chat-form', require('./components/ChatForm.vue').default);
*/
Vue.component('image-component', require('./components/ImageuploadComponent.vue').default);
Vue.component('my-component', require('./components/MyComponent.vue').default);



/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.mixin({
    data() {
        return {
            base_url : window.location.protocol + '//' + window.location.hostname + '/bite_circle/'
        }
    }
});

const app = new Vue({
    el: '#app',

  /*  data: {
        messages: []
    },

    created() {
        this.fetchMessages();
        Echo.private('chat')
		  .listen('MessageSent', (e) => {
		    this.messages.push({
		      message: e.message.message,
		      user: e.user
		    });
          });
    },

    methods: {
        fetchMessages() {
            axios.get(base_url + '/messages').then(response => {
                this.messages = response.data;
            });
        },

        addMessage(message) {
            this.messages.push(message);

            axios.post(base_url +'/messages', message).then(response => {
              console.log(response.data);
            });
        },
        getBaseUrl(){

        }
    }*/

});
