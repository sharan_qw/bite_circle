@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Cities</div>

                <div class="card-body">
                    <a href="{{ url('admin/cities/create') }}" class="btn btn-sm btn-primary">Add new</a>
                    <br><br>
                    <table class="table">   
                        <tr>
                            <th>name</th>
                            <th></th>
                        </tr>
                        @forelse($cities as $city)
                            <tr>
                                <td>{{ $city->name }}</td>
                                <td>
                                    <a href="{{ url('admin/cities/' .$city->id.'/edit') }}" class="btn btn-sm btn-info">Edit</a>
                                </td>  
                                <td>  
                                    <form method="post" action="/admin/cities/{{ $city->id }}">
                                        
                                     
                                        @csrf
                                        {{method_field('DELETE')}}
                                        <input type="submit" value="Delete" onclick="return confirm('Are you sure?')" class="btn btn-sm btn-danger " >
                                    </form>
                                </td>    
                            </tr>
                            @empty
                            <tr>
                                <td colspan="2">No record found.</td>
                            </tr>
                        @endforelse
                        
                    </table>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
