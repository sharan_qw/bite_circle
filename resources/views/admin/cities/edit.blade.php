@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit city</div>

                <div class="card-body">

                   <form method="post" action="{{ url('admin/cities', $city->id) }}">
                    {{ method_field('PUT')}}
                    @csrf

                    Name:
                    <input type="text" name="name" value="{{ $city->name }}" class="form-control"><br>

                    <input type="submit" value="save" class="btn btn-primary">
                       
                   </form>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
