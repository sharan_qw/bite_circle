<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Bite Circle') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('public/js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.1/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('public/css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Bite Circle') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                            @if(!Auth::guard('admin')->user())
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                        {{--<div>
                            <a href="#section2"><button class="btn btn-outline-warning"><i class="fa fa-shopping-cart"></i></button></a>
                        </div>--}}
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle text-dark" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::guard('admin')->user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>

                        @endguest
                    </ul>
                </div>
            </div>
        </nav>



        <main class="py-4">

            <div class="row">
                <div class="col-md-3">
                    <ul>

                        <a href= "{{url('/admin/dashboard-new')}}" class="nav-link btn btn-outline-warning">
                            <i class="nav-icon fas fa-utensils"></i>
                            <h5>Dashboard</h5>
                        </a>

                        <br/>
                        <a href= "{{url('admin/registered')}}" class="nav-link btn btn-outline-warning">
                            <i class="nav-icon fas fa-utensils"></i>
                            <h5>Registered Users</h5>
                        </a>

                        <br/>


                        <a href= "{{url('admin/dishes')}}" class="nav-link btn btn-outline-warning">
                            <i class="nav-icon fas fa-utensils"></i>
                            <h5>Menu</h5>
                        </a>

                    <br/>


                        <a href= "{{url('/admin/dishes/create')}}" class="nav-link btn btn-outline-warning">
                            <i class="nav-icon far fa-user"></i>
                            <h5>Add Dishes</h5>
                        </a>

                    <br/>


                        <a href= "{{url('/admin/last-order')}}" class="nav-link btn btn-outline-warning">
                            <i class="fas fa-paper-plane"></i>
                            <h5>Last Orders</h5>
                        </a>
                        <br/>
                        <a href= "{{url('/admin/category')}}" class="nav-link btn btn-outline-warning">
                            <i class="nav-icon fas fa-utensils"></i>
                            <h5>Category</h5>
                        </a>

                        <br/>

                        <!-- </li>
                         <li class="nav-item">
                             <a href="/order-history" class="nav-link">
                                 <i class="nav-icon fas fa-utensils"></i>
                                 <h5>orderrrrrrrrrr</h5>
                             </a>
                         </li>-->
                    </ul>
                </div>



        @yield('content')

        </div>
    </main>
</div>
</body>
</html>
