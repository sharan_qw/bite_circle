@extends('admin.layouts.app')

@section('content')


    <div class="col-md-9">
        <div class="card">
            <div class="card-header"> Edit Category</div>

            <div class="card-body">

                <form method="post" action="{{ url('admin/category/update/'.$categories->id) }}" enctype="multipart/form-data">
                    {{ method_field('PUT')}}
                    @csrf
                    Name:
                    <input type="text" name="name" value="{{ $categories->name }}" class="form-control"><br>

                    <input type="submit" value="save" class="btn btn-primary">

                </form>

            </div>
        </div>
    </div>
    </div>

@endsection
