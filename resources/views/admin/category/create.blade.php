@extends('admin.layouts.app')

@section('content')
    <div class="col-md-9">
        <div class="card">
            <div class="card-header">Add New Category</div>

            <div class="card-body">

                <form method="post" action="{{ url('admin/category/make') }}">
                @csrf
                Name:
                <input type="text" name="name" class="form-control"><br>

                <input type="submit" value="save" class="btn btn-primary">
                </form>

            </div>
        </div>
    </div>
    </div>

@endsection
