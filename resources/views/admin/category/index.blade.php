@extends('admin.layouts.app')

@section('content')
    <div class="col-md-9">

    {{--<div class="container">--}}
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">

                    <div class="card-header">Category</div>

                    <div class="card-body">
                        <a href="{{ url('admin/category/create_cat') }}" class="btn btn-sm btn-primary">Add new</a>
                        <br><br>

                       <table class="table">

                            <tr>
                                <th>Name of Categories</th>
                                <th colspan="2" style="text-align: center;">Actions</th>
                            </tr>

                            @forelse($category as $cat)
                                <tr>
                                    <td>{{ $cat->name }}</td>
                                    <td>
                                        <a href="{{ url('admin/category/' .$cat->id. '/edit') }}" class="btn btn-success">Edit</a>
                                    </td>
                                    <td><form method="post" action="{{ url('admin/category/destroy/'.$cat->id) }}" enctype="multipart/form-data">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger" type="submit">Delete</button>

                                        </form>
                                    </td>
                           @empty
                               <tr>
                                   <td colspan="2">No record found.</td>
                               </tr>





                                </tr>

                            @endforelse

                        </table>
                        {{$category->links()}}

                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">

            @if(session()->get('success'))

                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
        </div>
    </div>
@endsection
