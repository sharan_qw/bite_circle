@extends('admin.layouts.app')

@section('content')


        <div class="col-md-9">
            <div class="card">
                <div class="card-header"> EDIT DISH</div>

                <div class="card-body">

                    <form method="post" action="{{ url('admin/dishes/update/'.$dish->id) }}" enctype="multipart/form-data">
                        {{ method_field('PUT')}}
                        @csrf
                        Name:
                        <input type="text" name="name" value="{{ $dish->name }}" class="form-control"><br>

                        Category:
                        <select name="category_id" id="" class="form-control">
                            @foreach ($categories as $category)
                                <option value="{{ $category->id,$dish->category_id }}"> {{ $category->name }} </option>
                            @endforeach
                        </select>

                        Description:
                        <input type="text" name="description" value="{{ $dish->description }}" class="form-control"><br>

                        Price:
                        <input type="text" name="price" value="{{ $dish->price}}" class="form-control"><br>

                        Status:
                        <select name="status" class="form-control" >
                            <option value="0">Inactive</option>
                            <option value="1">Active</option>
                        </select><br>

                        Image:<br>
                        <input type="file" name="image" value="{{ $dish->image}}"><br><br>


                        <input type="submit" value="save" class="btn btn-primary">

                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection
