@extends('admin.layouts.app')

@section('content')

        <div class="col-md-9">
            <table class="table table-bordered">
                        <tr>
                            <th>Name</th>
                            <th>Category Name</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>Status</th>
                            <th>Image</th>
                            <th colspan="2" style="text-align: center;">Actions</th>

                        </tr>


                        @foreach($dishes as $dish)

                            <tr>
                                <td>{{$dish->name}}</td>
                                <td>{{$dish->category_name}}</td>
                                <td>{{$dish->description}}</td>
                                <td>{{$dish->price}} </td>
                                <td>
                                    @if($dish->status =='1')
                                        Active
                                    @else
                                        Inactive
                                    @endif    
                                </td>
                                <td>
                                    @if(!empty($dish->image))
                                        <img src="{{url('public/images/'. $dish->image)}}" style="height: 70px;width: 90px"/> </td>
                                    @else
                                        <h5>No Image Found </h5>
                                        @endif
                                <td>
                                    <a href="{{ url('admin/dishes/' .$dish->id. '/edit') }}" class="btn btn-success">Edit</a>
                                </td>
                                    <td><form method="post" action="{{ url('admin/dishes/destroy/'.$dish->id) }}" enctype="multipart/form-data">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger" type="submit">Delete</button>

                                        </form></td>
                            </tr>
                        @endforeach


            </table>
            {{$dishes->links()}}

        </div>
        <div class="col-md-12">

            @if(session()->get('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
        </div>

@endsection


