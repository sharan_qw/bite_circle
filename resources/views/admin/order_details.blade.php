@extends('admin.layouts.app')

@section('content')

        <div class="col-md-9">
            <div class="card">
                <div class="card-header">Order Details for {{ $user->username }}</div>
                <h3>
                </h3>
                <div class="card-body">
                    <table class="table table-bordered">
                        <tr>

                            <th>Dish Name </th>
                            <th>Quantity</th>
                            <th>Price</th>
                            <th>SubTotal</th>
                        </tr>
                        @foreach($order_details as $order_details)
                        <tr>
                            <td>{{$order_details->dish_name}}</td>
                            <td>{{$order_details->quantity}}</td>
                            <td>{{$order_details->price}}</td>
                            <td>{{$subtotal = $order_details->price * $order_details->quantity}}</td>
                        </tr>

                        @endforeach
                    </table>
                    <table>

                        <tr>
                            <td>Total:- &nbsp; {{ $total}} &#8377;</td>
                            

                        </tr>
                    </table>

                </div>
            </div>
        </div>
    </div>
@endsection
