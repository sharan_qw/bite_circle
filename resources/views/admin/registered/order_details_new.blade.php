@extends('admin.layouts.app')

@section('content')

    <div class="col-md-9">
        <div class="card">
            <div class="card-header">Order Details for {{ $user->username }}</div>
            <h3>
                &nbsp;  &nbsp;  &nbsp;Order Id : {{$order_details[0]->pid}}
            </h3>
            <div class="card-body">
                <table class="table table-bordered">
                    <tr>

                        <th>Dish Name</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>SubTotal</th>
                    </tr>
                    {{--@foreach($order as $order_details)--}}
                    @foreach($order_details as $order_detail)
                        <tr>
                            <td>{{$order_detail->dish_name}}</td>
                            <td>{{$order_detail->quantity}}</td>
                            <td>{{$order_detail->price}}</td>
                            <td>{{$subtotal = $order_detail->price * $order_detail->quantity}}</td>
                        </tr>
                            {{--@endforeach--}}
                    @endforeach
                </table>
                <table>
                    <tr>
                        <td>Total:- &nbsp; {{ $total}} &#8377;</td>


                    </tr>
                </table>

            </div>
        </div>
    </div>
    </div>
@endsection
