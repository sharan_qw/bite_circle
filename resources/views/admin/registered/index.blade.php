@extends('admin.layouts.app')

@section('content')
    <div class="col-md-9">
        <table class="table table-bordered">
            <tr>
                <th>Name of User</th>
                <th>Email</th>
                <th>No. of Orders</th>

            </tr>


                @foreach($users as $user)

                <tr>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td><a href="order_details_old/{{$user->id}}">{{$user->no_of_orders}}</a></td>

                </tr>
                    @endforeach
        </table>
        {{$users->links()}}
    </div>

@endsection


