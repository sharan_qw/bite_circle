@extends('admin.layouts.app')

@section('content')

    <div class="col-md-9">
        <div class="card">
            <div class="card-header">Order Details</div>
            <h3>
            </h3>
            <div class="card-body">
                <table class="table table-bordered">
                    <tr>
                        <th>Order id</th>
                    </tr>
                    {{--@foreach($order as $order_details)--}}
                    @foreach($placed_orders as $order)
                        <tr>
                            <td><a href="order_details_new/{{$order->id}}">{{$order->id}}</a></td>
                        </tr>
                        {{--@endforeach--}}
                    @endforeach
                </table>

            </div>
        </div>
    </div>
    </div>
@endsection
