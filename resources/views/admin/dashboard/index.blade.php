@extends('admin.layouts.app')

@section('content')

    {{--<div class="container">--}}

    {{--<div class="container">--}}
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><h2>Chart Links</h2></div>

                    <div class="panel-body">
                        <table class="table table-bordered">

                            <tr>
                                <th>Orders Chart</th>
                                <th>Users chart</th>
                            </tr>

                                <tr>

                                    <td> <a href="{{ url('admin/order_chart') }}" class="btn btn-sm btn-primary">Orders chart</a></td>
                                    <td> <a href="{{ url('admin/users_chart') }}" class="btn btn-sm btn-primary">Users chart</a></td>
                                </tr>


                        </table>
                    </div>
                </div>
            </div>
        </div>
        {{--</div>--}}
    {{--</div>--}}

@endsection
