@extends('admin.layouts.app')

@section('content')

        <div class="col-md-9">
            <div class="card">
                <div class="card-header">Last orders</div>
                <div class="card-body">
                    
                    <table class="table table-bordered">
                        <tr>

                           <th>Order Id</th>
                            <th>User</th>
                            <th>Placed On</th>
                            <th>Action</th>


                        </tr>
                        @foreach($placed_orders as $placed_order)
                        <tr>
                            <td>{{$placed_order->id}}</td>

                            <td>{{$placed_order->user_name}}</td>
                            <td>{{$placed_order->created_at}}</td>
                            <td>
                                <a href="{{ url('admin/order-details/'.$placed_order->id )}}" classs="btn btn-success">View Order Details</a>
                            </td>





                        </tr>

	                    @endforeach
	                </table>
                    {{$placed_orders->links()}}
                </div>
            </div>
        </div>
    </div>

@endsection
