@extends('home')
@section('content')
<div class="pos-f-t">
    <div class="collapse" id="navbarToggleExternalContent">
        <div class="bg-dark p-4">
            <div style="text-align: center;"><h3 style="font-family: Chiller; color:white">Welcome to the world of Taste</h3></div>
            <h4 class="text-white" style=" padding: 20px;">
                @foreach ($categories as $category)
                    <a href= "{{url('/dishes_show/'.$category->id)}}" style="margin-left: 70px; color:white;">{{$category->name}}</a>
                @endforeach
                </h4>
            <span class="text-muted"></span>
        </div>
    </div>
    <nav class="navbar navbar-dark bg-dark">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
    </nav>
</div>
    <main>
        @yield('content-1')
    </main>
@endsection
