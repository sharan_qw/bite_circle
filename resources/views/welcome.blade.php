<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Bite Circle</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            background-image: url(https://images.wallpaperscraft.com/image/neon_inscription_cafe_124281_1366x768.jpg);

            background-repeat: no-repeat;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: left;
            color: white;
            transform: skewY(-15deg);
            font-size: 13px;


        }

        .title {
            font-size: 84px;
            float: left;
            position: relative;
            left: 240px;
        }

        .links > a {
            color: white;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;


        }

        .m-b-md {
            margin-bottom: 280px;
            font-family: "Matura MT Script Capitals";
        }

    </style>
</head>
<body>
<div class="col-md-4">
    <a href="{{ url('/home')}}" >
    <img src="{{ asset('public/upload/abc.png')}}" height="100" width="100">
    </a>
    <div class="col-8">
        @if (Route::has('login'))
        <div class="top-right links">
            <a href="/bite_circle/about">About</a>
            <a href="/bite_circle/home">Product</a>
            <a href="#">News</a>
            <a href="/bite_circle/contactus">Contact</a>
            @auth
            <a href="{{ url('/home') }}">Home</a>
            @else
            <a href="{{ route('login') }}">Login</a>


            @if (Route::has('register'))
            <a href="{{ route('register') }}">Register</a>

            @endif
            @endauth
        </div>
        @endif
    </div>

</div>

<div class="flex-center position-ref full-height">


    <div class="content">
        <div class="title m-b-md">
            Bite Circle
        </div>

    </div>
</div>
</div>

</body>
</html>
