@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">List all dishes</div>

                <div class="card-body">
                    <a href="{{ url('/dishes') }}" class="btn btn-primary">Add new</a>
                    <br><br>


                    <table class="table table-bordered">
                        <tr>
                            <th>Name</th>
                            <th>Category_id</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>Status</th>
                            <th>Image</th>
                            <th class="colspan-2">Action</th>
                        </tr>


                        @foreach($dishes as $dish)

                            <tr>
                                <td>{{$dish->name}}</td>
                                <td>{{$dish->category_name}}</td>
                                <td>{{$dish->description}}</td>
                                <td>{{$dish->price}} </td>
                                <td>
                                    @if($dish->status =='1')
                                        Active
                                    @else
                                        Inactive
                                    @endif    
                                </td>
                                <td>
                                    @if(!empty($dish->image))
                                        <img src="{{url('upload/',$dish->image)}}" style="height: 70px;width: 90px"/> </td>
                                    @else
                                        <h5>No Image Found </h5>
                                        @endif

                                <td>
                                    <a href="{{ url('/dishes/' .$dish->id. '/edit') }}" class="btn btn-success">Edit</a>
                                </td>
                            </tr>
                        @endforeach

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
