@extends('layouts.app')

@section('content')

<div>
    <div class="row" style="padding-left:30px ;">


        <div class="col-md-2">
            <br/>

            <div class="col-md-12">
                <div>
                    <a href="/bite_circle/home" class="nav-link btn btn-danger">
                        <i class="nav-icon fas fa-utensils"></i>
                        <h5 style="mar: 100px">Menu</h5>
                    </a>

                    <br/>


                    <a href="/bite_circle/profile" class="nav-link btn btn-danger">
                        <i class="nav-icon far fa-user"></i>
                        <h5>Profile</h5>
                    </a>

                    <br/>


                    <a href="/bite_circle/contactus" class="nav-link btn btn-danger">
                        <i class="fas fa-paper-plane"></i>
                        <h5>Contact Us</h5>
                    </a>
                    <br/>

                    {{--<a href="/bite_circle/chat" class="nav-link btn btn-danger">
                        <i class="fa fa-comment" aria-hidden="true"></i>
                        <h5>Chat</h5>
                    </a>--}}



                </div>

            </div>

        </div>

        <div class="col-md-10">
            @yield('dynamic-content')
        </div>

    </div>
    <div class="abcde">


        <a href="https://www.facebook.com/qualwebs/" class="facebook"><i class="fab fa-facebook-f"></i></a>
        <a href="https://www.instagram.com/qualwebs/" class="instagram"><i class="fab fa-instagram"></i></a>
        <a href="https://www.linkedin.com/company/qualwebs" class="linkedin"><i class="fab fa-linkedin-in"></i></a>


    </div>
</div>
@endsection



