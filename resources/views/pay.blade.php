@extends('layouts.app')

@section('content')

    <div id="main-content">
        <div class="container clear">
            <div class="panel-body" style="border: 1px solid #ddd;padding: 10px;background: #eee;width: 30%;">
                <form id="rzp-footer-form" name="footerForm" action="{!!route('dopayment')!!}" method="POST"
                      style="width: 100%; text-align: center">
                    @csrf

                    <img src="/images/{{$dish->image}}" height="150px" width="150px"/>
                    <br/>
                    <p><br/>


                        {{$dish->name}}
                    </p>
                    <input type="hidden" name="amount" id="amount" value="{{$dish->price}}"/>
                    <div class="pay">
                        <button class="razorpay-payment-button btn filled small" id="paybtn" type="button">Pay with
                            Razorpay
                        </button>
                    </div>
                </form>
                <br/><br/>
                <div id="paymentDetail" style="display: none">
                    <center>
                        <div>paymentID: <span id="paymentID"></span></div>
                        <div>paymentDate: <span id="paymentDate"></span></div>
                    </center>
                </div>
            </div>

        </div>
    </div>


@endsection
@section('page-script')
    <script>
        $('#rzp-footer-form').submit(function (e) {
            var button = $(this).find('button');
            var parent = $(this);
            button.attr('disabled', 'true').html('Please Wait...');
            $.ajax({
                method: 'get',
                url: this.action,
                data: $(this).serialize(),
                complete: function (r) {
                    console.log('complete');
                    console.log(r);
                }
            })
            return false;
        })
    </script>

    <script>
        function padStart(str) {
            return ('0' + str).slice(-2)
        }

        function demoSuccessHandler(transaction) {
            // You can write success code here. If you want to store some data in database.
            $("#paymentDetail").removeAttr('style');
            $('#paymentID').text(transaction.razorpay_payment_id);
            var paymentDate = new Date();
            $('#paymentDate').text(
                padStart(paymentDate.getDate()) + '.' + padStart(paymentDate.getMonth() + 1) + '.' + paymentDate.getFullYear() + ' ' + padStart(paymentDate.getHours()) + ':' + padStart(paymentDate.getMinutes())
            );

            $.ajax({
                method: 'post',
                url: "{!!route('dopayment')!!}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "razorpay_payment_id": transaction.razorpay_payment_id
                },
                complete: function (r) {
                    console.log('complete');
                    console.log(r);
                }
            })
        }
    </script>
    <script>
        var options = {
            key: "{{('rzp_test_7txDGt9UznQQxe')}}",
            amount: '11111',
            name: 'Bite Circle',
            description: 'Food item',
            image: 'https://images.wallpaperscraft.com/image/neon_inscription_cafe_124281_1366x768.jpg',
            handler: demoSuccessHandler
        }
    </script>
    <script>
        // window.r = new Razorpay(options);
        // document.getElementById('paybtn').onclick = function () {
        //     r.open()
        // }
    </script>
@endsection

