@extends('home')

@section('dynamic-content')
<div xmlns:Auth="http://www.w3.org/1999/xhtml">


<div class="row">
    <div class ="row" style="margin:20px; color:white;" >
    <h3><strong>{{ Auth::user()->name }}</strong></h3>
    </div>
  <!--  <img src="/storage/avatar/{{Auth::user()->avatar}}" class="rounded-circle"
         style="width:150px; height:150px;"/>-->
    <profile-component></profile-component>

</div>
</div>
@endsection
